This is bear bones implementation of Facebook, accompanied by a simulator consisting of a simulator made 
of Akka Actors in the millions/hundreds of thousands, simulating clients attempting to access and retrieve 
information from Facebook. This implementation also has an API based on spray to handle the requests from 
the clients. The goal is to handle as many of these requests as possible while keeping the system secured.